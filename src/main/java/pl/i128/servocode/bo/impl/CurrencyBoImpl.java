package pl.i128.servocode.bo.impl;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.i128.servocode.bo.CurrencyBo;
import pl.i128.servocode.dao.CurrencyDao;
import pl.i128.servocode.model.Currency;

@lombok.Setter
@Service
public class CurrencyBoImpl implements CurrencyBo {

	@Autowired
	private CurrencyDao currencyDao;

	public List<Currency> getAllCurrency() {
		return currencyDao.getAllCurrency();
	}

}
