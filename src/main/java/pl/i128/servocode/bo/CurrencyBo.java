package pl.i128.servocode.bo;

import java.math.BigDecimal;
import java.util.List;

import pl.i128.servocode.model.Currency;

public interface CurrencyBo {

	List<Currency> getAllCurrency();
	
}
