package pl.i128.servocode.model;

import java.io.Serializable;

@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.ToString
@lombok.EqualsAndHashCode
public class Currency implements Serializable {
	
	private static final long serialVersionUID = 6471808392152020426L;
	
	private String fullName;
	private String shortName;
}
