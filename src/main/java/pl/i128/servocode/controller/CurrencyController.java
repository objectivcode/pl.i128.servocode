package pl.i128.servocode.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;

import pl.i128.servocode.bo.CurrencyBo;

@Controller()
public class CurrencyController {

	@Autowired
	private CurrencyBo currencyBo;

	@RequestMapping(value = "/currency/index")
	public ModelAndView index() {

		ModelAndView modelAndView = new ModelAndView("currency/index");
		return modelAndView;

	}

	@RequestMapping(value = "/currency/getCurriences", method = RequestMethod.GET, headers = "Accept=application/json", produces = "application/json")
	@ResponseBody
	public String getCurriences() {

		Gson gson = new Gson();
		String jsonCurrency = gson.toJson(this.currencyBo.getAllCurrency());
		return jsonCurrency;
	}

}
