package pl.i128.servocode.constants;

@lombok.AllArgsConstructor
@lombok.Getter
public enum CurrencyList {
	EUR("EURO","EUR"),
	AUD("Dollar Australijski","AUD"),
	BGN("Lew Bulgarski","BGN"),
	BRL("Real Brazylijski","BRL"),
	CAD("Dolar Kanadyjski","CAD"),
	CHF("Frank Szwajcarski","CHF"),
	CNY("Juan","CNY"),
	CZK("Korona Czeska","CZK"),
	DKK("Korona Dunska","DKK"),
	GBP("Funt Szerling","GBP"),
	HKD("Dolar Hongkonski","HKD"),
	HRK("Kuna Chorwacka","HRK"),
	HUF("Forint Wegierski","HUF"),
	IDR("Rupia Indonezyjska","IDR"),
	ILS("Nowy Szekel Izrealski","ILS"),
	INR("Rupia Indyjska","INR"),
	JPY("Jen Japonski","JPY"),
	KRW("Won Poludniowokoreanski","KRW"),
	MXN("Peso Meksykanskie","MXN"),
	MYR("Ringgit Malezyjski","MYR"),
	NOK("Korona Norwerska","NOK"),
	NZD("Dolar Nowozelandzki","NZD"),
	PHP("Peso Filipinskie","PHP"),
	PLN("Zlotowka","PLN"),
	RON("Nowa Leja Rumunska","RON"),
	RUB("Rubel Rosyjski","RUB"),
	SEK("Korona Szwedzka","SEK"),
	SGD("Dolar Singapurski","SGD"),
	THB("Baht Tajski","THB"),
	TRY("Lira Turecka","TRY"),
	USD("Dolar Amerykanski","USD"),
	ZAR("Rand Poludniowoafrykanski","ZAR");
	
	private final String shortName;
	private final String fullName;
	
	
}
