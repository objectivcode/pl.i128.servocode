package pl.i128.servocode.dao;

import java.util.List;

import pl.i128.servocode.model.Currency;

public interface CurrencyDao {
	
	List<Currency> getAllCurrency();
}
