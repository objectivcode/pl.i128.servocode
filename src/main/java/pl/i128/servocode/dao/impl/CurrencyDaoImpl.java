package pl.i128.servocode.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import pl.i128.servocode.constants.CurrencyList;
import pl.i128.servocode.dao.CurrencyDao;
import pl.i128.servocode.model.Currency;

@Repository
public class CurrencyDaoImpl implements CurrencyDao {

	public List<Currency> getAllCurrency() {
		List<Currency> currencyList = new ArrayList<Currency>();

		for (CurrencyList item : CurrencyList.values()) {

			Currency currency = new Currency();
			currency.setFullName(item.getFullName());
			currency.setShortName(item.getShortName());

			currencyList.add(currency);
		}

		return currencyList;
	}

}
