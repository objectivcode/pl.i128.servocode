package pl.i128.servocode.test;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import static org.mockito.Mockito.*;

import junit.framework.TestCase;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;

import pl.i128.servocode.bo.CurrencyBo;
import pl.i128.servocode.controller.CurrencyController;
import pl.i128.servocode.model.Currency;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/config/Config.xml")
@WebAppConfiguration
public class CurrencyControllerTest extends TestCase {

	private MockMvc mockMvc;

	@Autowired
	CurrencyBo currencyBo;

	@Autowired
	WebApplicationContext webApplicationContext;


	@Before
	public void init() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).dispatchOptions(true).build();
	}
	
	@Test
	public void test_return_json_1() {
		CurrencyController currencyControllerMock = mock(CurrencyController.class);
		
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("PLN", "Zlotowka");
		
		when(currencyControllerMock.getCurriences()).thenReturn(jsonObject.toString());
		assertEquals(currencyControllerMock.getCurriences(), jsonObject.toString());
		
	}
	

	// I have problem: WARNING: No mapping found for HTTP request with URI [/currency/index] in DispatcherServlet with name ''
	// And I cannot create this test.	
	@Test
	public void test_get_curriences() throws Exception {

		mockMvc.perform(post("/currency/index"))
				.andExpect(status().isOk());

	}

}
