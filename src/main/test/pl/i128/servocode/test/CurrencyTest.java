package pl.i128.servocode.test;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import pl.i128.servocode.model.Currency;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/config/Config.xml")
public class CurrencyTest {

	@Test
	public void test_to_string_currency() {
		
		Currency currency = new Currency();
		currency.setShortName("PLN");
		currency.setFullName("Zlotowka");
		
		String expected = "Currency(fullName=Zlotowka, shortName=PLN)";
		
        assertEquals(expected, currency.toString());
	}
}
