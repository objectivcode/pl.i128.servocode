package pl.i128.servocode.test;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import junit.framework.TestCase;
import pl.i128.servocode.bo.CurrencyBo;
import pl.i128.servocode.bo.impl.CurrencyBoImpl;
import pl.i128.servocode.model.Currency;

import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/config/Config.xml")
public class CurrencyBoImplTest extends TestCase {

	@Autowired
	CurrencyBo currencyBo;
	
	@Test 
	public void test_get_all_currency() {
		assertNotNull(currencyBo.getAllCurrency());
	}
	
	@Test 
	public void test_get_all_currency_size() {
		assertEquals(currencyBo.getAllCurrency().size(),32);
	}
	
	@Test
	public void test_type_of_currency() {
		CurrencyBo currencyBoMock = mock(CurrencyBoImpl.class);
		
		Currency currency = new Currency();
		currency.setShortName("PLN");
		currency.setFullName("Zlotowka");
		
		when(currencyBoMock.getAllCurrency()).thenReturn(Arrays.asList(currency));
		assertEquals(currencyBoMock.getAllCurrency().get(0).getShortName(), "PLN");
	}

}
