module.exports = function ( config ) {
    config.set( {
        basePath         : '../../../../',
        frameworks       : ['jasmine'],
        files            : [
            'src/main/webapp/resources/bower_components/angular/angular.js',
            'src/main/webapp/resources/bower_components/angular-mocks/angular-mocks.js',
            'src/main/webapp/resources/bower_components/angular-resource/angular-resource.js',
            'src/main/webapp/resources/bower_components/angular/angular.js',
            'src/main/webapp/resources/js/**/*.js',
            'src/main/test/js/**/*.js',
            
        ],
        exclude          : [],
        preprocessors    : {
            'src/main/webapp/resources/js/**/*.js' : ['coverage']
        },
        reporters        : ['progress', 'coverage'],
        port             : 9876,
        colors           : true,
        logLevel         : config.LOG_INFO,
        autoWatch        : true,
        browsers         : ['Chrome'],
        singleRun        : false,
        plugins          : [
            'karma-jasmine',
            'karma-chrome-launcher',
            'karma-phantomjs-launcher',
            'karma-junit-reporter',
            'karma-coverage'
        ],
        coverageReporter : {
            type : 'html',
            dir  : 'target/coverage/'
        }
    } );
};