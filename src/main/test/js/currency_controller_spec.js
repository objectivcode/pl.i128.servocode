describe("CurrencyController", function() {

	beforeEach(angular.mock.module('CurrencyManagement'));

	var $http;
	var $httpBackend;
	var controller;
	var scope;

	beforeEach(angular.mock.inject(function(_$http_, _$httpBackend_) {
		$http = _$http_;
		$httpBackend = _$httpBackend_;
	}));

	beforeEach(angular.mock.inject(function(_$controller_, $rootScope) {
		scope = $rootScope.$new();
		controller = _$controller_('CurrencyController', {
			$scope : scope
		});
	}));

	describe("Initial Scenario", function() {
		it('should attach a list of currencyList', function() {
			expect(scope.currencyList.length).toBe(0);
		});

		it('should init parameter for scope.from field', function() {
			expect(scope.from).toBe(null);
		});

		it('should init parameter for scope.to field', function() {
			expect(scope.from).toBe(null);
		});

		it('should init parameter for scope.values field', function() {
			expect(scope.from).toBe(null);
		});
	});

	/**
	 * I will please you Stanislaw, to talk about this method. 
	 */
	describe("calling a submit calculator", function() {

		beforeEach(function() {
			scope.from = "EUR";
			scope.to = "PLN";
			scope.values = 1;
			scope.submitCalculator();
		});

		it('check correct calculate', function() {

			expect(scope.result).not.toEqual(null);
		});

	});

});
