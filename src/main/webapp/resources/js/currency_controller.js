'use strict'

app.controller("CurrencyController", function CurrencyController($scope, $http) {
	$scope.currencyList = [];
	_loadCurriencesList();
	
	$scope.from = null;
	$scope.to = null;
	$scope.values = null;
	$scope.result = null;
		
	function _loadCurriencesList() {
		$http({
			method : 'GET',
			url : '/servocode/currency/getCurriences'
		}).then(function successCallback(response) {
			$scope.currencyList = response.data;
		}, function errorCallback(response) {
			alert(response.statusText);
		});
	}

	function _round(value, decimals) {
		return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
	}
	
	function _returnAmountAfterConvert($scope,response) {
		$scope.result = _round($scope.values * response.data.rates[$scope.to], 2) + " " + $scope.to;
	}
	
	$scope.submitCalculator = function() {

			$http({
				method : 'GET',
				url : 'http://api.fixer.io/latest?base=' + $scope.from
			}).then(function successCallback(response) {
				if ($scope.from != $scope.to) {
					_returnAmountAfterConvert($scope,response);
					alert($scope.result);
				} else {
					alert('You cannot choose this same type of currency.');
				}

			}, function errorCallback(response) {
				alert(response.statusText);
			});

	}

});