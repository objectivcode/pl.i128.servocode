<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css"
	integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M"
	crossorigin="anonymous">


<script src="${pageContext.request.contextPath}/resources/bower_components/angular/angular.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/app.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/currency_controller.js"></script>




</head>
<body ng-app="CurrencyManagement" ng-controller="CurrencyController">
	<center style="margin-top: 200px; margin-left: 200px;">
		<form name="currencyForm" ng-submit="submitCalculator()">

			<div class="form-row align-items-center">
				<div class="col-auto">

					<div class="form-group">
						<label for="currencyFrom">Select Currency From</label> <select
							class="form-control" name="from" ng-model="from" ng-required="true">
							<option value="" selected="selected">Select</option>
							<option ng-repeat="option in currencyList" ng-selected="$first"
								value="{{option.fullName}}">{{option.shortName}}</option>
						</select>
					</div>

				</div>
				<div class="col-auto">
					<div class="form-group">
						<label for="currencyTo">Select Currency To</label> <select
							class="form-control" name="to" ng-model="to" ng-required="true">
							<option value="" selected="selected">Select</option>
							<option ng-repeat="option in currencyList"
								value="{{option.fullName}}">{{option.shortName}}</option>
						</select>
					</div>
				</div>
				<div class="col-auto">
				
					<div class="form-group">
						<label for="currency.values">Enter Amount</label> <input
							type="number" class="form-control" name="values"
							ng-model="values" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01" ng-required="true"
							ng-minlength="1"
							ng-maxlength="6">
					</div>					
				</div>
				<div class="col-auto">
					<button type="submit" class="btn btn-primary">Calculate</button>
				</div>
			</div>
		</form>
	</center>
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
		integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"
		integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1"
		crossorigin="anonymous"></script>

</body>
</html>
